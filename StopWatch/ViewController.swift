//
//  ViewController.swift
//  StopWatch
//
//  Created by Ola Sundbo Halvorsen on 28/10/2015.
//  Copyright © 2015 olahalvorsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer:NSTimer?
    var msSinceStart:Int = 5000
    
    var miliSeconds = 0
    var seconds:Int = 0
    var minutes:Int = 0
    var hours:Int = 0
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBAction func start(sender: AnyObject) {
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "update", userInfo: nil, repeats: true)
        
    }
    
    @IBAction func stop(sender: AnyObject) {
        if(timer != nil) {
            timer!.invalidate()
            timer = nil;
        }
    }
    
    @IBAction func reset(sender: AnyObject) {
        stop(self)
        msSinceStart = 0
        updateUIClock()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // must be internal or public.
    func update() {
        msSinceStart += 1
        updateUIClock()
    }
    
    func updateUIClock() {
        miliSeconds = msSinceStart % 100
        seconds = (msSinceStart / 100) % 60
        minutes = (msSinceStart / 100) / 60
        
        let milisecondsString = String(format: "%02d", miliSeconds)
        let secondsString = String(format: "%02d", seconds)
        let minutesString = String(format: "%02d", minutes)
        
        timeLabel.text = minutesString + ":" + secondsString + ":" + milisecondsString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

